path = require 'path'

options =
  port: process.env.PORT or 3000
  appRoot: __dirname
  root: path.join(__dirname, '..')

app = require('./config/server')(options)
Glass = require('./config/glass')(app, options)
require('./config/routes').apply(Glass, [options])

Glass.app.listen app.get('port'), ->
  console.log "Express listening on port #{app.get('port')}"
