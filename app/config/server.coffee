express = require 'express'
path = require 'path'
ECT = require 'ect'

module.exports = (options) ->
  viewRoot = path.join(options.appRoot, 'views')
  ectRender = ECT(
    watch: true
    root: viewRoot
    # ext: '.ect'
  ).render

  app = express()

  app.configure ->

    app.use express.favicon()
    app.use express.static("#{__dirname}/app")
    app.use app.router
    app.set 'port', options.port or 3000
    app.set 'views', viewRoot

    app.engine('ect', ectRender)
    app.engine('html', ectRender)

    app.set('view engine', 'ect')

  return app

