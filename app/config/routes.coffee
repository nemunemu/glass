module.exports = (options) ->
  express = require 'express'
  path = require 'path'
  _ = require 'underscore'
  Mincer = require 'mincer'

  Mincer.CoffeeEngine.configure
    bare: false

  Mincer.logger.use
    log:    console.log
    debug:  console.log
    info:   console.log
    warn:   console.log
    error:  console.log

  @app.use express.logger('dev')

  @app.get '/', (req, res) =>
    res.render 'index.ect',
      homedir: @homedir

  @app.get '/hello', (req, res) ->
    res.send {message : "Hello World"}

  @app.get '/directory/:uuid', (req, res, next) =>
    { uuid } = req.params
    @request('DirectoriesController', 'show', [uuid], req, res)

  @app.get '/source/:uuid', (req, res, next) =>
    { uuid } = req.params
    @request('SourcesController', 'show', [uuid], req, res)

  @app.use express.static(path.join(options.root, '.tmp'))
  @app.use express.static(path.join(options.root, 'public'))
  @app.use(
    '/bower_components',
    express.static(path.join(options.appRoot, 'bower_components')))

  environment = new Mincer.Environment()
  environment.appendPath 'app/assets/javascripts'
  @app.use('/assets', Mincer.createServer(environment))

  @app.use (req, res) =>
    @render404(req, res)

