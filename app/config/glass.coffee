module.exports = (app, options) ->
  _ = require('underscore')

  Glass =
    utils: {}
    Model: {}
    Controller: {}
    Policies: {}

    request: (controllerName, method, params, req, res) ->
      controller = new (Glass.requireController(controllerName))()
      ErrorPageException = @requireUtil('ErrorPageException')

      try
        if typeof controller[method] is 'function'
          res.send(controller[method](params...))
        else
          throw new ErrorPageException
            message: "unknown controller method: #{method}"
      catch error
        if error instanceof ErrorPageException
          console.log("error #{error.status}: (#{error.message})")
          if error.status is 404
            @render404(req, res)
          else
            res.status(error.status)
            res.send(error.message)
        else
          throw error

    requireController: (controller) ->
      @requireComponent("api.controllers.#{controller}")

    requireModel: (model) ->
      @requireComponent("api.models.#{model}")

    requireUtil: (util) ->
      @requireComponent("utils.#{util}")

    requireComponent: (namespace) ->
      @loadedModules ||= {}
      @loadingModules ||= {}
      loadPath = "#{@appRoot}/#{@namespaceToModulePath(namespace)}"

      # load module's definition once
      if @loadedModules[loadPath]
        @loadedModules[loadPath]
      else
        if @loadingModules[loadPath]
          throw new Error("Circular dependency detected: #{loadPath}")
        @loadingModules[loadPath] = true
        @loadedModules[loadPath] = require(loadPath).apply(this)
        @loadingModules[loadPath] = undefined
        @loadedModules[loadPath]

    importUtils: ->
      @requireUtil('property').import()

    namespaceToModulePath: (name) ->
      _.map(name.split("\."), (str) => @underscore(str)).join("/")

    underscore: (str) ->
      str
        .replace(/[A-Z]/g, (str) -> "_#{str}")
        .replace(/^\_/g, '')
        .toLowerCase()

    resetModules: ->
      @loadingModules = {}

    render404: (req, res) ->
      res.status(404)
      res.render('404.html')

  _.extend(Glass, options)
  Glass.app = app
  Glass.importUtils()

  Directory = Glass.requireModel('Directory')
  Glass.homedir = new Directory(process.env.HOME)

  return Glass
