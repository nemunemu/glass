
module.exports = ->
  class @Policies.FiletypeDetectionPolicy
    constructor: (@file) ->

    filetype: ->
      switch @file.extension
        when 'js' then 'javascript'
        when 'coffee' then 'coffeescript'
        when 'rb' then 'ruby'
        when 'htm', 'html' then 'html'
        else ''

