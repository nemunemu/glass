
module.exports = ->
  File = @requireModel('File')
  class @Model.Source extends File
    fs = require('fs')

    @property 'content',
      get: ->
        @_content ||= fs.readFileSync(@path, 'utf8')

    toJsonWithContent: ->
      @toJson(@jsonKeys.concat('content', 'filetype'))

