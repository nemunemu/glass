module.exports = ->
  fs = require('fs')
  path = require('path')
  File = @requireModel('File')

  class @Model.Directory extends File
    isDirectory: true

    toJsonWithEntries: ->
      @toJson(@jsonKeys.concat('entries'))

    @property 'entries',
      get: ->
        _ = require('underscore')
        @_entries ||= _.map(fs.readdirSync(@path), (basepath) =>
          File.build(path.join(@path, basepath))
        )

