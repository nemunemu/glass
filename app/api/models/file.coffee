module.exports = ->
  fs = require('fs')
  path = require('path')
  glass = this

  UuidModule = @requireUtil('UuidModule')
  FiletypeDetectionPolicy = @requireComponent('api.policies.FiletypeDetectionPolicy')

  return class @Model.File extends UuidModule
    @findByUuid: (uuid) ->
      @build(super)

    @build: (path) ->
      console.log("reading: #{path}")
      if fs.existsSync(path)
        stat = fs.statSync(path)
        if stat.isDirectory()
          Directory = glass.requireModel('Directory')
          new Directory(path)
        else
          Source = glass.requireModel('Source')
          new Source(path)

    constructor: (@path) ->

    toJson: (jsonKeys = @jsonKeys) ->
      _ = require('underscore')
      _.chain(jsonKeys)
        .reduce((json, key) ->
          value = @[key]
          json[key] =
            if _.isArray(value)
              _.map(value, (v) -> v.toJson?() || v)
            else
              value.toJson?() || value
          json
        , {}, this)
        .value()


    getJsonKeys: -> [
      'path'
      'uuid'
      'isDirectory'
    ]

    isDirectory: false

    @property 'uuid',
      get: ->
        @constructor.uuidFromPath(@path)

    @property 'jsonKeys',
      get: -> @getJsonKeys()

    @property 'extension',
      get: -> path.extname(@path).split('.').pop()

    @property 'filetype',
      get: -> (new FiletypeDetectionPolicy(this)).filetype()

