module.exports = ->
  Directory = @requireModel('Directory')
  Controller = @requireController('Controller')
  ErrorPageException = @requireUtil('ErrorPageException')

  return class @Controller.DirectoriesController extends Controller
    show: (_uuid) ->
      @directory.toJsonWithEntries()

    getUuid: (_uuid, entryName) ->
      if entry = @directory.getUuidOfEntry(entryName)
        uuid: entry.uuid
      else
        throw new ErrorPageException
          message: 'entry.notFound'

    @beforeAction ['show', 'getUuid'], (uuid) ->
      unless @directory = Directory.findByUuid(uuid)
        throw new ErrorPageException
          message: 'directory.notFound'

