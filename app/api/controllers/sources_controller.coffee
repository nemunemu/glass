module.exports = ->
  Source = @requireModel('Source')
  Controller = @requireController('Controller')
  ErrorPageException = @requireUtil('ErrorPageException')

  class @Controller.SourcesController extends Controller
    show: (uuid) ->
      @source.toJsonWithContent()

    @beforeAction ['show'], (uuid) ->
      unless @source = Source.findByUuid(uuid)
        throw new ErrorPageException
          message: 'notFound'

