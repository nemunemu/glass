module.exports = ->
  return class @Controller
    constructor: ->
      @beforeActions.forEach ({ condition, action }) =>
        (condition.methods || []).forEach (method) =>
          original_method = @[method]
          @["#{method}_without_filter"] = original_method
          @[method] = @_filter(original_method, action)

    @beforeAction: (methods, action) ->
      @prototype.beforeActions ||= []
      filter = @_makeActionRule(methods, action)
      @prototype.beforeActions.push(filter)

    @_makeActionRule: (methods, action) ->
      methods = [methods] unless methods instanceof Array
      condition:
        methods: methods
      action: action

    _filter: (method, action) ->
      =>
        action.apply(this, arguments)
        method.apply(this, arguments)

