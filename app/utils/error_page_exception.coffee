module.exports = ->
  @Exceptions ||= {}

  return class @Exceptions.ErrorPageException
    status: 404
    message: 'exception'

    constructor: (options = {}) ->
      @status = options.status || @status
      @message = options.message || @message

