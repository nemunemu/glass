module.exports =
  namespace: (namespace) ->
    parts = namespace.split('.')
    _.reduce(parts, (layer, part) ->
      layer[part] ||= {}
    , this)
