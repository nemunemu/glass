module.exports = ->
  return mixin = (superclass, modules...) ->
    class SuperClass extends superclass
    modules.each (module) ->
      _.mixin(SuperClass, module)
      _.mixin(SuperClass.prototype, module.prototype)
    SuperClass

