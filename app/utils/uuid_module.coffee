
module.exports = ->
  qs = require 'querystring'

  return class @utils.UuidModule
    @findByUuid: (uuid) ->
      @pathFromUuid(uuid)

    @pathFromUuid: (uuid) ->
      qs.unescape(uuid)

    @uuidFromPath: (path) ->
      qs.escape(path)

