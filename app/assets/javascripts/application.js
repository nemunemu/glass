//= require config
//= require glass
//= require ace_extensions

//= require_tree ./backbone/models
//= require_tree ./backbone/views
//= require_tree ./backbone/routers
//= require_tree ./backbone/collections

$(document).ready(function () {
    Backbone.history.start();
});
