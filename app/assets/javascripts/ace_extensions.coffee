define 'ace/ace_extensions', ['require', 'exports', 'module'],
(require, exports, module) ->
  oop = require("./lib/oop")
  EditSession = require("./edit_session").EditSession
  SearchHighlight = require("./search_highlight").SearchHighlight

  EditSessionExtensions =
    highlightCurrentWord: (word) ->
      @removeMarker(@$currentWordMarker.id) if @$currentWordMarker
      if word
        highlight = new SearchHighlight(word, "ace_selection", "text")
        @$currentWordMarker = @addDynamicMarker(highlight)

    removeCurrentWordHighlight: ->
      @removeMarker(marker.id) if marker = @$currentWordMarker

  oop.implement(EditSession.prototype, EditSessionExtensions)

require ['ace/ace_extensions'], ->
