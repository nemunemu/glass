#= require_tree ./filemenu

namespace = Glass.namespace('views')

class namespace.FilemenuView extends Backbone.View
  initialize: ->
    @render()

  buildRootView: ->
    @rootView ||= namespace.filemenu.FileView.build
      model: @model
      el: $("<li id=\"#{@model.viewId}\"></li>")

  render: ->
    unless @$el.html()
      @$el.html("<h1 class=\"filemenu-root\">#{@model.path}</h1>")
      @$el.append($("<ul class=\"root\"></ul>"))
      @$el.children('ul.root').append(@buildRootView().render().el)
    this
