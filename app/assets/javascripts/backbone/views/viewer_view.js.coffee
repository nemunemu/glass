namespace = Glass.namespace('views')

class namespace.ViewerView extends Backbone.View
  events:
    'click': 'onClick'

  initialize: ->
    @editor = ace.edit(@el)
    @editor.container.style.opacity = ''
    @editor.setReadOnly(true)
    @editor.setHighlightSelectedWord(true)
    @editor.setOptions
      maxLines: 1000,
      autoScrollEditorIntoView: true

  open: (model) ->
    @changeModel(model)
    @model.on('change', @render, this)
    @model.fetch()

  changeModel: (model) ->
    @model.stopListening() if @model
    @model = model

  render: ->
    @editor.setValue(@model.content)
    @editor.getSession().setMode("ace/mode/#{@model.get('filetype')}")

  onClick: ->
    return unless @editor
    { row, column } = @editor.getCursorPosition()
    word = @editor.getSession().doc.getTextRange(@editor.selection.getWordRange()).trim()
    @editor.getSession().highlightCurrentWord(word)
    @model.getDefinitions(word) if @model

