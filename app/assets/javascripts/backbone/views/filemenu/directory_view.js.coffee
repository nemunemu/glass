#= require backbone/models/files/directory
#= require ./file_view.js.coffee

namespace = Glass.namespace('views.filemenu')

class namespace.DirectoryView extends namespace.FileView
  events:
    'click > div.title': 'toggle'

  initialize: ->
    @model.entries.on('add', @addChild, this)

  addChild: (model) ->
    html = "<li id=\"#{model.viewId}\"></li>"
    childView = namespace.FileView.build
      model: model
      el: $(html)

    @childViews ||= []
    @childViews.push(childView)

    @$el.children('ul.children').append(childView.render().el)

  expand: ->
    return if @isExpanded
    @render()
    @model.fetch()
    @isExpanded = true
    @$el.children('ul.children').show()

  collapse: ->
    return unless @isExpanded
    @isExpanded = false
    @$el.children('ul.children').hide()

  toggle: ->
    if @isExpanded then @collapse() else @expand()

  render: ->
    @$el.addClass('directory') unless @$el.hasClass('directory')
    if @$el.html()
      @$el.children('div.title').html(@model.basepath)
    else
      @$el.html("<div class=\"title\">#{@model.basepath}</div>")
      @$el.append($("<ul class=\"children\"></ul>").hide())
    this

