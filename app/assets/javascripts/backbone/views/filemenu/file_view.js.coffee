namespace = Glass.namespace('views.filemenu')

class namespace.FileView extends Backbone.View
  @build: (options) ->
    if options.model.get('isDirectory')
      new namespace.DirectoryView(options)
    else
      new namespace.SourceView(options)

  @property 'viewId',
    get: ->
      uuid = @uuid.replace(/[^\w\-]/g, '-')
      "file-#{uuid}"
