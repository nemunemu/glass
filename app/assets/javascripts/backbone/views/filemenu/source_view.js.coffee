#= require backbone/models/files/source
#= require ./file_view.js.coffee

namespace = Glass.namespace('views.filemenu')

class namespace.SourceView extends namespace.FileView
  events:
    'click > div.title': 'open'

  open: ->
    Glass.openSource(@model)

  render: ->
    @$el.addClass('source') unless @$el.hasClass('source')
    if @$el.html()
      @$el.children('div.title').html(@model.basepath)
    else
      @$el.html("<div class=\"title\">#{@model.basepath}</div>")
    this

