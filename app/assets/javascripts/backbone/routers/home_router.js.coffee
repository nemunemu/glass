#= require backbone/views/viewer_view
#= require backbone/views/filemenu_view
#= require backbone/models/files/directory

class HomeRouter extends Backbone.Router
  routes:
    '': 'home'

  home: ->
    @editor = new Glass.views.ViewerView
      el: $('div#viewer')

    Glass.registerWorkspace(@editor)

    $('#toggle-sidr').sidr
      name: 'filemenu'
      source: -> ''

    homedir = JSON.parse($('#homedir').html())

    @filemenu = new Glass.views.FilemenuView
      model: new Glass.models.files.Directory(homedir)
      el: '#filemenu'

Glass.namespace('routers').HomeRouter = new HomeRouter
