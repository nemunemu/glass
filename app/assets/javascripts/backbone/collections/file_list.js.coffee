#= require backbone/models/file

namespace = Glass.namespace('collections')

class namespace.FileList extends Backbone.Collection
  model: Glass.models.File

