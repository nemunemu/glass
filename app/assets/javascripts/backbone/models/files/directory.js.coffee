#= require backbone/models/file
#= require backbone/collections/file_list

namespace = Glass.namespace('models.files')

class namespace.Directory extends Glass.models.File
  urlRoot: '/directory'
  isDirectory: true

  initialize: ->
    @entries = new Glass.collections.FileList

  set: (key, val, options) ->
    if typeof key is 'object'
      attrs = key
      options = val
    else
      (attrs = {})[key] = val
    options ||= {}

    if entries = attrs['entries']
      @entries.set(entries)
    super(_.omit(attrs, 'entries'), options)

