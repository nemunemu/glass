namespace = Glass.namespace('models.files')

class namespace.Source extends Glass.models.File
  urlRoot: '/source'

  @property 'content',
    get: -> @get('content')

  getDefinitions: (word) ->

