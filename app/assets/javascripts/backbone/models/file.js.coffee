namespace = Glass.namespace('models')

class namespace.File extends Backbone.Model
  urlRoot: '/file'
  idAttribute: 'uuid'

  constructor: (attrs) ->
    if @constructor is namespace.File
      if attrs.isDirectory
        return new namespace.files.Directory(arguments...)
      else
        return new namespace.files.Source(arguments...)
    else
      super

  @property 'content',
    get: ->

  @property 'basepath',
    get: -> _.last(@path.split('/'))
    set: ->

  @property 'path',
    get: -> unescape(@uuid)

  @property 'uuid',
    get: -> @get('uuid')

  @property 'viewId',
    get: ->
      uuid = @uuid.replace(/[^\w\-]/g, '-')
      "file-#{uuid}"
