Glass =
  namespace: (namespace) ->
    parts = namespace.split('.')
    _.reduce(parts, (layer, part) ->
      layer[part] ||= {}
    , this)

  registerWorkspace: (workspace) ->
    workspace.on('focus', @setCurrentWorkspace, this)
    workspace.on('remove', @unRegisterWorkspace, this)

    @currentWorkspace ||= workspace
    @workspaces || = []
    @workspaces.push(workspace)

  setCurrentWorkspace: (workspace) ->
    @currentWorkspace = workspace

  unRegisterworkspace: (workspace) ->

  openSource: (@model) ->
    return unless @currentWorkspace
    @currentWorkspace.open(@model)

_.extend(Glass, Backbone.Events)

window.Glass = Glass
