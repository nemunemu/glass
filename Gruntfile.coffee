"use strict"
LIVERELOAD_PORT = 35729
SERVER_PORT = 9000
# lrSnippet = require("connect-livereload")(port: LIVERELOAD_PORT)
# mountFolder = (connect, dir) ->
#   connect.static require("path").resolve(dir)


# # Globbing
# for performance reasons we're only matching one level down:
# 'test/spec/{,*/}*.js'
# use this if you want to match all subfolders:
# 'test/spec/**/*.js'
# templateFramework: 'lodash'
module.exports = (grunt) ->

  # show elapsed time at the end
  require("time-grunt") grunt

  # load all grunt tasks
  require("load-grunt-tasks") grunt

  # configurable paths
  yeomanConfig =
    app: "app"
    dist: "dist"

  grunt.initConfig
    yeoman: yeomanConfig
    watch:
      options:
        nospawn: true
        livereload: true

      coffee:
        files: ["<%= yeoman.app %>/scripts/{,*/}*.coffee"]
        tasks: ["coffee:dist"]

      coffeeTest:
        files: ["test/spec/{,*/}*.coffee"]
        tasks: ["coffee:test"]

      compass:
        files: ["<%= yeoman.app %>/styles/{,*/}*.{scss,sass}"]
        tasks: ["compass"]

      livereload:
        options:
          livereload: LIVERELOAD_PORT

        files: [
          "<%= yeoman.app %>/*.html"
          "{.tmp,<%= yeoman.app %>}/styles/{,*/}*.css"
          "{.tmp,<%= yeoman.app %>}/scripts/{,*/}*.js"
          "<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp}"
          "<%= yeoman.app %>/scripts/templates/*.{ejs,mustache,hbs}"
          "test/spec/**/*.js"
        ]

      jst:
        files: ["<%= yeoman.app %>/scripts/templates/*.ejs"]
        tasks: ["jst"]

      test:
        files: [
          "<%= yeoman.app %>/scripts/{,*/}*.js"
          "test/spec/**/*.js"
        ]
        tasks: ["test:true"]

    express:
      options:
        port: SERVER_PORT
        hostname: 'localhost'
        script: 'app/app.coffee'
        cmd: 'coffee'

      dev: {}

      test:
        options:
          port: 9001

    # connect:
    #   options:
    #     port: SERVER_PORT

    #     # change this to '0.0.0.0' to access the server from outside
    #     hostname: "localhost"

    #   livereload:
    #     options:
    #       middleware: (connect) ->
    #         [
    #           lrSnippet
    #           mountFolder(connect, ".tmp")
    #           mountFolder(connect, yeomanConfig.app)
    #         ]

    #   test:
    #     options:
    #       port: 9001
    #       middleware: (connect) ->
    #         [
    #           lrSnippet
    #           mountFolder(connect, ".tmp")
    #           mountFolder(connect, "test")
    #           mountFolder(connect, yeomanConfig.app)
    #         ]

    #   dist:
    #     options:
    #       middleware: (connect) ->
    #         [mountFolder(connect, yeomanConfig.dist)]

    open:
      server:
        path: "http://localhost:<%= express.options.port %>"

      test:
        path: "http://localhost:<%= express.test.options.port %>"

    clean:
      dist: [
        ".tmp"
        "<%= yeoman.dist %>/*"
      ]
      server: ".tmp"

    jshint:
      options:
        jshintrc: ".jshintrc"
        reporter: require("jshint-stylish")

      all: [
        "Gruntfile.js"
        "<%= yeoman.app %>/scripts/{,*/}*.js"
        "!<%= yeoman.app %>/scripts/vendor/*"
        "test/spec/{,*/}*.js"
      ]

    mocha:
      all:
        options:
          run: true
          urls: ["http://localhost:<%= connect.test.options.port %>/index.html"]

    coffee:
      dist:
        files: [

          # rather than compiling multiple files here you should
          # require them into your main .coffee file
          expand: true
          cwd: "<%= yeoman.app %>/scripts"
          src: "{,*/}*.coffee"
          dest: ".tmp/scripts"
          ext: ".js"
        ]

      test:
        files: [
          expand: true
          cwd: "test/spec"
          src: "{,*/}*.coffee"
          dest: ".tmp/spec"
          ext: ".js"
        ]

    compass:
      options:
        sassDir: "<%= yeoman.app %>/styles"
        cssDir: ".tmp/styles"
        imagesDir: "<%= yeoman.app %>/images"
        javascriptsDir: "<%= yeoman.app %>/scripts"
        fontsDir: "<%= yeoman.app %>/styles/fonts"
        importPath: "<%= yeoman.app %>/bower_components"
        relativeAssets: true

      dist: {}
      server:
        options:
          debugInfo: true

    # requirejs:
    #   dist:

    #     # Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
    #     options:

    #       # `name` and `out` is set by grunt-usemin
    #       baseUrl: ".tmp/scripts"
    #       optimize: "none"
    #       paths:
    #         templates: "../../.tmp/scripts/templates"
    #         jquery: "../../app/bower_components/jquery/jquery"
    #         underscore: "../../app/bower_components/underscore/underscore"
    #         backbone: "../../app/bower_components/backbone/backbone"
    #         ace: "../../app/bower_components/ace/ace"


    #       # TODO: Figure out how to make sourcemaps work with grunt-usemin
    #       # https://github.com/yeoman/grunt-usemin/issues/30
    #       #generateSourceMaps: true,
    #       # required to support SourceMaps
    #       # http://requirejs.org/docs/errors.html#sourcemapcomments
    #       preserveLicenseComments: false
    #       useStrict: true
    #       wrap: true

    # bower:
    #   all:
    #     rjsConfig: "<%= yeoman.app %>/scripts/main.js"

    jst:
      options:
        amd: true

      compile:
        files:
          ".tmp/scripts/templates.js": ["<%= yeoman.app %>/scripts/templates/*.ejs"]

  grunt.registerTask 'dryice', ->
    { buildAce } = require("#{yeomanConfig.app}/bower_components/ace/Makefile.dryice.js")
    buildAce({})

  grunt.registerTask "createDefaultTemplate", ->
    grunt.file.write ".tmp/scripts/templates.js", "this.JST = this.JST || {};"
    return

  grunt.registerTask "server", ->
    grunt.log.warn "The `server` task has been deprecated. Use `grunt serve` to start a server."
    grunt.task.run ["serve:" + target]
    return

  grunt.registerTask "serve", (target) ->
    if target is "dist"
      return grunt.task.run([
        "build"
        "open:server"
        #   "connect:dist:keepalive"
      ])
    if target is "test"
      return grunt.task.run([
        "clean:server"
        "coffee"
        "createDefaultTemplate"
        "jst"
        "compass:server"
        # "connect:test"
        "open:test"
        "watch:livereload"
      ])
    grunt.task.run [
      "clean:server"
      "coffee:dist"
      "createDefaultTemplate"
      "jst"
      "compass:server"
      #    "connect:livereload"
      'express:dev'
      # "open:server"
      "watch"
    ]
    return

  grunt.registerTask "test", (isConnected) ->
    isConnected = Boolean(isConnected)
    testTasks = [
      "clean:server"
      "coffee"
      "createDefaultTemplate"
      "jst"
      "compass"
      # "connect:test"
      "mocha"
      "watch:test"
    ]
    unless isConnected
      grunt.task.run testTasks
    else

      # already connected so not going to connect again, remove the connect:test task
  #    testTasks.splice testTasks.indexOf("connect:test"), 1
      grunt.task.run testTasks

  grunt.registerTask "build", [
    "clean:dist"
    "coffee"
    "createDefaultTemplate"
    "jst"
    "compass:dist"
    # "useminPrepare"
    "requirejs"
    #   "imagemin"
    #   "htmlmin"
    "concat"
    # "cssmin"
    # "uglify"
    # "copy"
    # "rev"
    # "usemin"
  ]
  grunt.registerTask "default", [
    "jshint"
    "test"
    "build"
  ]
  return
